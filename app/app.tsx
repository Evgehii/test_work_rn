import { AppRegistry } from "react-native"
import {
  createAppContainer
} from 'react-navigation'
import RootStackNavigator from "./navigation/navigator"

const App = createAppContainer(RootStackNavigator)

AppRegistry.registerComponent('liftlog_rn', () => App)
