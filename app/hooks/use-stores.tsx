import React from 'react'
import { storesContext } from '../context-stores'

export const useStores = () => React.useContext(storesContext)
