import React from 'react'
import * as S from './input-text-style'

interface Props {
  onChangeText:(text)=>void
  value:string
}

const Input:React.FC<Props> = (props) => {
  return (
    <S.Input
      onChangeText={text => props.onChangeText(text)}
      value={props.value}
    />
  )
}

export default Input
