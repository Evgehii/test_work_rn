import styled from 'styled-components/native'

export const Input = styled.TextInput`
  height: 40px;
  border: 1px solid black;
  align-self: stretch;
  margin: 20px;
  border-radius: 10px;
  padding: 10px;
`
