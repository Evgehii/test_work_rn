import React from 'react'
import { LoginStore } from "../screens/login/login-store"
import { PersonalStore } from "../screens/personal/personal-store"

export const storesContext = React.createContext({
  loginStore: new LoginStore(),
  PersonalStore: new PersonalStore(),
})
