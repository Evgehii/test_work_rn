import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import * as Types from "./api.types"

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        Accept: "application/json",
        access_token:'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgzYjJhOTk1ZmU4ZGI0YjkwYjhmYWU5N2ZhMjAwOTA1YWNiMzU4YTQ4Y2UzZmU0NTQ2ZDUyZTdlNDg0NWY2OWIxYmI4NzFkMjI5MjJkNmU2In0.eyJhdWQiOiIxIiwianRpIjoiODNiMmE5OTVmZThkYjRiOTBiOGZhZTk3ZmEyMDA5MDVhY2IzNThhNDhjZTNmZTQ1NDZkNTJlN2U0ODQ1ZjY5YjFiYjg3MWQyMjkyMmQ2ZTYiLCJpYXQiOjE1NzYxNjk0MzYsIm5iZiI6MTU3NjE2OTQzNiwiZXhwIjoxNjA3NzkxODM2LCJzdWIiOiIxNSIsInNjb3BlcyI6W119.f0lGItHsk-5-t3atR3hrRuaEYN0pwWgIORlaEXhO3lzwxYJFjyiOBiG5cwYaoQmKhYvhsmgYlNODiJsqi_2-KaWeiOdNEEFFmd4iSFL0Ja-jGYNkky8XnX-2IRcD5shuZ4EzH6ae8l6u-yp8r1P1ohbetOP8zXUY2UxVxrNY42I-tnRX4BSoH7F8d0HVQuoC9LroeGhivEIeiBI8ZIbLE4qKncc8E-3tR2V92inWa_l5fQPipIG1DqWd8j9zUWKO8rrRY8dZ5KGoa10Vi3LhcEIcQkM7r6ZNjVvO0YFeSlVRcg-WMHHP3-o6sSc0e92fxOj8p2BQ1bwvKseF_AXfhBDXT5TQRg-y-sCH-JF-Wue2E8QJHeCmrObi0jC5PB_Z4NNPF0rIFCZYs1Z_5Gs4zZY-MGoGl5b_S5vK00XyndVEZavLmdvKmTV2s7PjdpJ7dwIp5i-liEPKq5RZbf6aKXnZ5aETQ97ZzIGdsEuFQesxUWaaliG64Mi18Mm2mNKw2uycO3Fy8wJp32xlplQOqdcFfbnWreThOSwcYBSCYh9gZ00qDrgx6UOYErN88uWt71qtzf6JKFmptx6_0Z-Zj8w6XUBpMUKeEKxCVVaE2aO9eaS8bUHnsl6h3DWoZdlQ99T_6i8reWiA9T82FeGREVZURzVJI20au98VZ0X7nso'
      }
    })
  }

  /**
   * Gets a list of users.
   */
  async getUsers(): Promise<Types.GetUsersResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const convertUser = raw => {
      return {
        id: raw.id,
        name: raw.name,
      }
    }

    // transform the data into the format we are expecting
    try {
      const rawUsers = response.data
      const resultUsers: Types.User[] = rawUsers.map(convertUser)
      return { kind: "ok", users: resultUsers }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Gets a single user by ID
   */

  async getUser(id: string): Promise<Types.GetUserResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users/${id}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const resultUser: Types.User = {
        id: response.data.id,
        name: response.data.name,
      }
      return { kind: "ok", user: resultUser }
    } catch {
      return { kind: "bad-data" }
    }
  }
}
