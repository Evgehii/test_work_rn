import { action, observable } from 'mobx'

export class PersonalStore {
  @observable
  theme = 'light'

  @action
  setTheme(newTheme: string) {
    this.theme = newTheme
  }
}
