import React from 'react'
import * as S from './login-style'
import Input from "../../components/input-text/input-text"
import { useObserver } from "mobx-react-lite"
import { useStores } from '../../hooks/use-stores'
import { Button } from "../../components"

export const LoginScreen = () => {
  const { loginStore } = useStores()
  return useObserver(() => (
    <S.Container>
      <S.Text>
        ВХОД
      </S.Text>
      <Input onChangeText={loginStore.setValLogin} value={loginStore.login}/>
      <Input onChangeText={loginStore.setValPassword} value={loginStore.password}/>
      <Button text={'Отправить'} onPress={loginStore.authenticate} />
    </S.Container>
  ))
}
