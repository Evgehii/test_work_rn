import { action, observable } from 'mobx'
import { Api } from "../../services/api"
import { ApiResponse } from "apisauce"

export class LoginStore {
  @observable login: string = ''

  @observable password: string = ''

  @action setValLogin = (login: string) => {
    this.login = login
  }

  @action setValPassword = (password: string) => {
    this.password = password
  }

  @action authenticate = async () => {
    const api = new Api()
    console.log('log--s ', api)
    api.setup()
    const response: ApiResponse<any> = await api.apisauce.get(`/api/user`, { login: this.login, password: this.password })
    console.log('response-- ', response)
    // // the typical ways to die when calling an api
    // if (!response.ok) {
    //   const problem = getGeneralApiProblem(response)
    //   if (problem) return problem
    // }

    // transform the data into the format we are expecting
    // try {
    //   // const resultUser: Types.User = {
    //   //   id: response.data.id,
    //   //   name: response.data.name,
    //   // }
    //   return { kind: "ok" }
    // } catch {
    //   return { kind: "bad-data" }
    // }
  }
}
