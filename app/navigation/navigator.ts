import { createBottomTabNavigator } from 'react-navigation'
import { LoginScreen, PersonalScreen } from "../screens"

const RootStackNavigator = createBottomTabNavigator({
  Login: {
    screen: LoginScreen
  },
  Personal: {
    screen: PersonalScreen
  },
},
{
  initialRouteName: 'Login'
}
)

export default RootStackNavigator
